<?php

return array(
    'lng.test' => 'texto simpre en portugues',
    'lng.txt.titleartist' => 'Artista',
    'lng.txt.welcome'=>'Artista',
    'lng.txt.message' => 'É um prazer ter você aqui',
    'lng.txt.new' => 'Novo',
    'lng.txt.edit' => 'Editar',
    'lng.txt.list' => 'Lista',
    'lng.txt.view' => 'Visão',

//formulario
    'lng.txt.ftitle'=>'Artista',
        'lng.txt.name' => 'Nome',
    'lng.txt.lastname'=>'Último nome',
    'lng.txt.typeart' =>'Tipo de arte',
    'lng.txt.experience' => 'Anos de experiência',
    'lng.txt.freelance' => 'eu trabalho sozinho',
    'lng.txt.corp'=>'Eu trabalho para uma empresa',
    'lng.txt.ok'=>'Está bem',
    'lng.txt.edit'=>'Editar'
);